require_dependency "expeerience_theme_plugin_constraint"

ExpeerienceThemePlugin::Engine.routes.draw do
  get "/" => "expeerience_theme_plugin#index", constraints: ExpeerienceThemePluginConstraint.new
  get "/actions" => "actions#index", constraints: ExpeerienceThemePluginConstraint.new
  get "/actions/:id" => "actions#show", constraints: ExpeerienceThemePluginConstraint.new
end
