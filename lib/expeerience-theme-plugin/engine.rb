module ExpeerienceThemePlugin
  class Engine < ::Rails::Engine
    engine_name "ExpeerienceThemePlugin".freeze
    isolate_namespace ExpeerienceThemePlugin

    config.after_initialize do
      Discourse::Application.routes.append do
        mount ::ExpeerienceThemePlugin::Engine, at: "/expeerience-theme-plugin"
      end
    end
  end
end
