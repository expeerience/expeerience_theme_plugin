# frozen_string_literal: true

# name: ExpeerienceThemePlugin
# about: Expeerience Theme
# version: 0.1
# authors: frank3manuel@gmail.com
# url: https://github.com/frank3manuel@gmail.com



enabled_site_setting :expeerience_theme_plugin_enabled

PLUGIN_NAME ||= 'ExpeerienceThemePlugin'

load File.expand_path('lib/expeerience-theme-plugin/engine.rb', __dir__)

after_initialize do
  # https://github.com/discourse/discourse/blob/master/lib/plugin/instance.rb
end
