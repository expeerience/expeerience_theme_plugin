module ExpeerienceThemePlugin
  class ExpeerienceThemePluginController < ::ApplicationController
    requires_plugin ExpeerienceThemePlugin

    before_action :ensure_logged_in

    def index
    end
  end
end
