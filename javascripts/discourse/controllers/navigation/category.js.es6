import { none, and } from "@ember/object/computed";
import NavigationDefaultController from "discourse/controllers/navigation/default";
import FilterModeMixin from "discourse/mixins/filter-mode";
import discourseComputed from "discourse-common/utils/decorators";

export default NavigationDefaultController.extend(FilterModeMixin, {
  showingParentCategory: none("category.parentCategory"),
  showingSubcategoryList: and(
    "category.show_subcategory_list",
    "showingParentCategory"
  ),
  
  @discourseComputed("category")
  isCategory(category) {
    if(!category){
      return false;
    } 
    return true;
  },
  
  @discourseComputed("category.can_edit")
  showCategoryEdit: (canEdit) => canEdit,

  @discourseComputed()
  categories() {
    return this.site.get("categoriesList");
  },
  
  @discourseComputed("category.subcategories")
  isParent(subcategories) {
    return subcategories && subcategories.length > 0;
  },

});
