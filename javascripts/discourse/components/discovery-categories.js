import Component from "@ember/component";
import UrlRefresh from "discourse/mixins/url-refresh";
import { on } from "discourse-common/utils/decorators";
import Scrolling from "discourse/mixins/scrolling";
const CATEGORIES_LIST_BODY_CLASS = "categories-list";

export default Component.extend(UrlRefresh,  Scrolling, {

  classNames: ["contents"],
  didInsertElement() {
    this._super(...arguments);
    $("body").addClass(CATEGORIES_LIST_BODY_CLASS);
    this.bindScrolling({ name: "dashboard-view" });
    this.hidesearch(true)
    $(window).on("scroll.dashboard-view", (e) => this.scrolled(e));
    


  },

  willDestroyElement() {
    this._super(...arguments);
    $("body").removeClass(CATEGORIES_LIST_BODY_CLASS);
    this.hidesearch(false);
    $(window).unbind("scroll.dashboard-view");
  },
  
  scrolled(e){
    const offset = window.pageYOffset || $("html").scrollTop();
    if(offset < 10){
      this.hidesearch(true)
    } else {
      this.hidesearch(false)
    }
  },

  hidesearch(setbool){
    this.appEvents.trigger("header:hide-search", setbool);
  }
});
