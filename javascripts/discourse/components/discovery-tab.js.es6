import I18n from "I18n";
import discourseComputed from "discourse-common/utils/decorators";
import { scheduleOnce } from "@ember/runloop";
import Component from "@ember/component";
import { propertyEqual } from "discourse/lib/computed";
import DiscourseURL from "discourse/lib/url";

export default Component.extend({
  tagName: "li",
  classNameBindings: ["active", "tabClassName"],
  
  @discourseComputed("tab")
  tabClassName(tab) {
    return "discovery-" + tab;
  },

  active: propertyEqual("selectedTab", "tab"),

  @discourseComputed("tab") 
  title(tab) {
    try{
      return I18n.t("category.navigation." + tab.replace("-", "_"), {count: this.count});
    } catch (e) {
      return I18n.t("category.navigation." + tab.replace("-", "_"));
    }

  },

  didInsertElement() {
    this._super(...arguments);

    // scheduleOnce("afterRender", this, this._addToCollection);

      if(this.type == this.tab){
        this.set("selectedTab", this.tab);
      } 

      if((this.type == 'regular' || !this.type) && this.tab == 'discussion'){
        this.set("selectedTab", this.tab);
      }
  },

  _addToCollection: function () {
    this.panels.addObject(this.tabClassName);
  },

  _resetWindowPosition() {
    const $modalBody = $('body')
    if ($modalBody.length === 1) {
      $modalBody.scrollTop(0);
    }
  },

  actions: {
    select: function () {
      if(this.tab == "about" && this.selectedTab != this.tab){
        let slug = this.category.topic_url
        DiscourseURL.routeTo(slug);
      } else if (this.tab == "discussion" ){
        DiscourseURL.routeTo(this.category.url );
      } else if (this.tab == "members"){
        let slug = "/g/" + this.category.group.name
        DiscourseURL.routeTo(slug);
      }
      this.set("selectedTab", this.tab);
      this._resetWindowPosition();
    }
  }
});
