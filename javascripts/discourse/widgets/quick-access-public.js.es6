import I18n from "I18n";
import { h } from "virtual-dom";
import QuickAccessPanel from "discourse/widgets/quick-access-panel";
import { createWidgetFrom } from "discourse/widgets/widget";
import { Promise } from "rsvp";

const _extraItems = [];

export function addQuickAccessProfileItem(item) {
  _extraItems.push(item);
}

createWidgetFrom(QuickAccessPanel, "quick-access-public", {
  tagName: "div.quick-access-panel.quick-access-public",

  buildKey: () => "quick-access-public",

  hideBottomItems() {
    // Never show the button to the full profile page.
    return true;
  },

  findNewItems() {
    return Promise.resolve(this._getItems());
  },

  itemHtml(item) {
    return this.attach("quick-access-item", item);
  },

  _getItems() {
    let items = []
    items.push(
        {
            content: "Log in ",
            icon:"sign-in-alt",
            action: "showLogin",
        },
       
    )
    if(!this.siteSettings.invite_only){
      items.push ({
        content: "Sign up",
        action: "showCreateAccount",
        icon: "user-plus",
      })
    }
    return items;
  },


  _showToggleAnonymousButton() {
    return (
      (this.siteSettings.allow_anonymous_posting &&
        this.currentUser.trust_level >=
          this.siteSettings.anonymous_posting_min_trust_level) ||
      this.currentUser.is_anonymous
    );
  },
});
