import { withPluginApi } from "discourse/lib/plugin-api";
import { setDefaultHomepage } from 'discourse/lib/utilities';

export default {
  name: "initialize-theme",
  initialize() {
    // debugger;
    if (this.siteSettings.home_url_override) {
      setDefaultHomepage(settings.home_url_override);
    }

    withPluginApi("0.8", api => {
      const splitIcons = this.siteSettings.icons_to_override.split("|").filter(Boolean)
      splitIcons.forEach(icon =>{
        const fragments = icon.split(",").map(fragment => fragment.trim());
        const oldIcon = fragments[0]
        const newIcon = fragments[1]
        api.replaceIcon(oldIcon, newIcon)      
      })
      
    });
  }
};
